# Author:	Louis Holbrook <dev@holbrook.no> 0826EDA1702D1E87C6E2875121D2E7BB88C2A746
# SPDX-License-Identifier:	GPL-3.0-or-later
# File-version: 1
# Description: Python interface to abi and bin files for faucet contracts

# standard imports
import logging
import json
import os

# external imports
from chainlib.eth.tx import TxFactory
from chainlib.eth.constant import ZERO_ADDRESS
from chainlib.eth.contract import (
        ABIContractEncoder,
        ABIContractDecoder,
        ABIContractType,
        abi_decode_single,
        )
from chainlib.jsonrpc import jsonrpc_template
from hexathon import add_0x

logg = logging.getLogger().getChild(__name__)

moddir = os.path.dirname(__file__)
datadir = os.path.join(moddir, 'data')


class TransferAuthorization(TxFactory):

    __abi = None
    __bytecode = None
   

    @staticmethod
    def abi():
        if TransferAuthorization.__abi == None:
            f = open(os.path.join(datadir, 'ERC20TransferAuthorization.json'), 'r')
            TransferAuthorization.__abi = json.load(f)
            f.close()
        return TransferAuthorization.__abi


    @staticmethod
    def bytecode():
        if TransferAuthorization.__bytecode == None:
            f = open(os.path.join(datadir, 'ERC20TransferAuthorization.bin'))
            TransferAuthorization.__bytecode = f.read()
            f.close()
        return TransferAuthorization.__bytecode


    @staticmethod
    def gas(code=None):
        return 2800000


    def constructor(self, sender_address):
        code = TransferAuthorization.bytecode()
        tx = self.template(sender_address, None, use_nonce=True)
        tx = self.set_code(tx, code)
        return self.build(tx)


    def signers(self, contract_address, signer_address, sender_address=ZERO_ADDRESS):
        o = jsonrpc_template()
        o['method'] = 'eth_call'
        enc = ABIContractEncoder()
        enc.method('signers')
        enc.typ(ABIContractType.ADDRESS)
        enc.address(signer_address)
        data = add_0x(enc.get())
        tx = self.template(sender_address, contract_address)
        tx = self.set_code(tx, data)
        o['params'].append(self.normalize(tx))
        o['params'].append('latest')
        return o


    def have_signer(self, contract_address, signer_address, sender_address=ZERO_ADDRESS):
        return self.signers(contract_address, signer_address, sender_address)


    @classmethod
    def parse_signers(self, v):
        return abi_decode_single(ABIContractType.BOOLEAN, v)


    @classmethod
    def parse_create_request_request(self, v):
        v = strip_0x(v)
        cursor = 0
        enc = ABIContractEncoder()
        enc.method('createRequest')
        enc.typ(ABIContractType.ADDRESS)
        enc.typ(ABIContractType.ADDRESS)
        enc.typ(ABIContractType.ADDRESS)
        enc.typ(ABIContractType.UINT256)
        r = enc.get()
        l = len(r)
        m = v[:l]
        if m != r:
            logg.error('method mismatch, expected {}, got {}'.format(r, m))
            raise RequestMismatchException(v)
        cursor += l

        dec = ABIContractDecoder()
        dec.typ(ABIContractType.ADDRESS)
        dec.typ(ABIContractType.ADDRESS)
        dec.typ(ABIContractType.ADDRESS)
        dec.typ(ABIContractType.UINT256)
        dec.val(v[cursor:cursor+64])
        cursor += 64
        dec.val(v[cursor:cursor+64])
        cursor += 64
        dec.val(v[cursor:cursor+64])
        cursor += 64
        dec.val(v[cursor:cursor+64])
        r = dec.decode()
        return r 

#
#    def last_serial(self):
#        return self.contract.functions.lastSerial().call()
#
#
#    def next_serial(self):
#        return self.contract.functions.nextSerial().call()
#
#
#    def requests(self, idx):
#        req = self.contract.functions.requests(idx).call()
#        return {
#            'serial': req[0],
#            'sender': req[1],
#            'recipient': req[2],
#            'token': req[3],
#            'value': req[4],
#                }
