# standard imports
import os
import unittest
import logging

# external imports
from chainlib.eth.nonce import RPCNonceOracle

# local imports
from erc20_transfer_authorization import TransferAuthorization

# testutil imports
from tests.base import TestBase

logg = logging.getLogger()

testdir = os.path.dirname(__file__)


class ERC20TransferAuthorizationBasicTest(TestBase):

    def test_basic(self):
        
        nonce_oracle = RPCNonceOracle(self.accounts[0], self.rpc)
        c = TransferAuthorization(chain_id=self.chain_spec.chain_id())

        o = c.signers(self.address, self.accounts[0], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertTrue(c.parse_signers(r))

        o = c.signers(self.address, self.accounts[1], sender_address=self.accounts[0])
        r = self.rpc.do(o)
        self.assertFalse(c.parse_signers(r))
    

#    def test_get(self):
#        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
#        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)
#
#        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
#        r = self.w3.eth.getTransactionReceipt(tx_hash)
#
#        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
#        log = r.logs[0]
#        topic = log.topics[0]
#        self.assertEqual(topic.hex()[2:], topic_match)
#        serial = int(log.data[66:], 16)
#
#        for i in range(1, 3):
#            tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3+i], t.address, 10*(i+1)).transact({'from': self.w3.eth.accounts[2]})
#
#        tq_count = w.functions.count().call()
#        assert (tq_count == 3);
#
#        for i in range(w.functions.nextSerial().call(), w.functions.lastSerial().call()):
#            tqg = w.functions.requests(i).call()
#     
#            self.assertEqual(tqg[0], i)
#            self.assertEqual(tqg[1], self.w3.eth.accounts[2])
#            self.assertEqual(tqg[2], self.w3.eth.accounts[3+(i-1)])
#            self.assertEqual(tqg[3], t.address)
#            self.assertEqual(tqg[4], 10*i)
#
#
#    def test_get_vote(self):
#        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
#        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)
#
#        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
#        w.functions.nay(1).transact({'from': self.w3.eth.accounts[0]})
#       
#        self.assertEqual(w.functions.voters(1, 0).call(), self.w3.eth.accounts[0])
#        self.assertEqual(w.functions.vote(1, self.w3.eth.accounts[0]).call(), -1)
#
#
#    def test_indexes(self):
#        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
#        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)
#
#        for i in range(0, 5):
#            amount = 10*(i+1)
#            sender = self.w3.eth.accounts[3+i];
#            tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], sender, t.address, amount).transact({'from': self.w3.eth.accounts[2]})
#
#        w.functions.nay(1).transact({'from': self.w3.eth.accounts[0]})
#        w.functions.nay(4).transact({'from': self.w3.eth.accounts[0]})
#
#        self.assertEqual(w.functions.getSerialAt(0).call(), 2)
#        self.assertEqual(w.functions.getSerialAt(1).call(), 3)
#        self.assertEqual(w.functions.getSerialAt(2).call(), 5)
#
#
#    def test_index_after_removal(self):
#        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
#        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)
#
#        for i in range(0, 4):
#            amount = 10*(i+1)
#            sender = self.w3.eth.accounts[3+i];
#            tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], sender, t.address, amount).transact({'from': self.w3.eth.accounts[2]})
#
#        tq_count = w.functions.count().call()
#        self.assertEqual(tq_count, 4);
#        self.assertEqual(w.functions.lastSerial().call(), 4)
#        self.assertEqual(w.functions.nextSerial().call(), 1)
#
#        w.functions.nay(2).transact({'from': self.w3.eth.accounts[0]})
#        self.assertEqual(w.functions.count().call(), 3)
#        self.assertEqual(w.functions.nextSerial().call(), 1)
#        self.assertEqual(w.functions.lastSerial().call(), 4)
#
#        w.functions.nay(1).transact({'from': self.w3.eth.accounts[0]})
#        self.assertEqual(w.functions.count().call(), 2)
#        self.assertEqual(w.functions.nextSerial().call(), 3)
#        self.assertEqual(w.functions.lastSerial().call(), 4)
#
#        w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 42).transact({'from': self.w3.eth.accounts[2]})
#        self.assertEqual(w.functions.count().call(), 3)
#        self.assertEqual(w.functions.lastSerial().call(), 5)
#        self.assertEqual(w.functions.nextSerial().call(), 3)
#
#        w.functions.nay(3).transact({'from': self.w3.eth.accounts[0]})
#        self.assertEqual(w.functions.count().call(), 2)
#        self.assertEqual(w.functions.lastSerial().call(), 5)
#        self.assertEqual(w.functions.nextSerial().call(), 4)
#
#        w.functions.nay(4).transact({'from': self.w3.eth.accounts[0]})
#        self.assertEqual(w.functions.count().call(), 1)
#        self.assertEqual(w.functions.nextSerial().call(), 5)
#        self.assertEqual(w.functions.lastSerial().call(), 5)
#
#        w.functions.nay(5).transact({'from': self.w3.eth.accounts[0]})
#        self.assertEqual(w.functions.count().call(), 0)
#        self.assertEqual(w.functions.nextSerial().call(), 0)
#        self.assertEqual(w.functions.lastSerial().call(), 5)
#
#
#    def test_pass_on_insufficient_approve(self):
#        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
#        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)
#
#        self.eth_tester.mine_block()
#
#        w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[2]})
#        w.functions.yay(1).transact({'from': self.w3.eth.accounts[0]})
#        self.eth_tester.mine_block()
#
#        tx_hash = w.functions.executeRequest(1).transact({'from': self.w3.eth.accounts[0]})
#
#        r = self.w3.eth.getTransactionReceipt(tx_hash)
#
#        topic_match = 'dab20a0fcd702cf875c2d715d5c3fc99af66a716c94b3405408c94b7311c99eb' # TransferFail(uint256)
#        log = r.logs[0]
#        topic = log.topics[0]
#        self.assertEqual(topic.hex()[2:], topic_match)
#        serial = int(log.data[2:], 16)
#
#        self.assertEqual(serial, 1)
#
#        self.assertEqual(w.functions.count().call(), 0)
#        self.assertEqual(w.functions.nextSerial().call(), 0)


if __name__ == '__main__':
    unittest.main()
