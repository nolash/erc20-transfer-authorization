# standard imports
import logging
import unittest

# testutil imports
from tests.base import TestBase

logg = logging.getLogger()

rejected_log_signature = '3d61d434b895790b08f040c45261fce3b3bec596278b3a0f25dd9f741d0ba469'
vetoed_log_signature = '1ad80b2541a1f52bdc838332d7c23606116a1188a8cbbc4c0948b4b56ce51d14'
approved_log_signature = '36ea04725f8aa40ee603224671681b753f9cba3cb5f67c5a0e24a3b39900c065'


class ERC20TransferAuthorizationQuorumTest(TestBase):

    def test_vote_access(self):
        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)

        w.functions.addSigner(self.w3.eth.accounts[5]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.setThresholds(2, 0).transact({'from': self.w3.eth.accounts[0]})

        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
        r = self.w3.eth.getTransactionReceipt(tx_hash)

        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)
        serial = int(log.data[66:], 16)
      
        # only signers may vote
        with self.assertRaises(Exception):
            tx_hashh = w.functions.yay(serial).transact({'from': self.w3.eth.accounts[2]})

        # only signers may vote
        with self.assertRaises(Exception):
            tx_hashh = w.functions.nay(serial).transact({'from': self.w3.eth.accounts[2]})

        w.functions.yay(serial).transact({'from': self.w3.eth.accounts[0]})

        # may not vote twice
        with self.assertRaises(Exception):
            tx_hashh = w.functions.yay(serial).transact({'from': self.w3.eth.accounts[0]})

        # may not change vote
        with self.assertRaises(Exception):
            tx_hashh = w.functions.nay(serial).transact({'from': self.w3.eth.accounts[0]})

        tx_hashh = w.functions.nay(serial).transact({'from': self.w3.eth.accounts[5]})

        # may not vote twice
        with self.assertRaises(Exception):
            tx_hashh = w.functions.nay(serial).transact({'from': self.w3.eth.accounts[5]})

        # may not change vote
        with self.assertRaises(Exception):
            tx_hashh = w.functions.yay(serial).transact({'from': self.w3.eth.accounts[5]})


    def test_minimal_quorum(self):
        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)

        t.functions.approve(w.address, 10).transact({'from': self.w3.eth.accounts[2]})
        self.eth_tester.mine_block()

        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
        r = self.w3.eth.getTransactionReceipt(tx_hash)

        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)
        serial = int(log.data[66:], 16)
      
        with self.assertRaises(Exception):
            w.functions.executeRequest(serial).transact({'from': self.w3.eth.accounts[0]})

        # only signers may vote
        with self.assertRaises(Exception):
            tx_hashh = w.functions.yay(serial).transact({'from': self.w3.eth.accounts[2]})

        tx_hashh = w.functions.yay(serial).transact({'from': self.w3.eth.accounts[0]})
        r = self.w3.eth.getTransactionReceipt(tx_hashh)

        topic_match = approved_log_signature
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)

        tx_hashh = w.functions.executeRequest(serial).transact({'from': self.w3.eth.accounts[0]})
        r = self.w3.eth.getTransactionReceipt(tx_hashh)

        topic_match = 'bcf6a68a2f901be4a23a41b53acd7697893a7e34def4e28acba584da75283b67' # Executed(serial)
        log = r.logs[1]
        topic = log.topics[0].hex()[2:]
        self.assertEqual(topic, topic_match)

        self.assertEqual(t.functions.balanceOf(self.w3.eth.accounts[2]).call(), 90)
        self.assertEqual(t.functions.balanceOf(self.w3.eth.accounts[3]).call(), 10)


    def test_simple_quorum(self):
        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)

        t.functions.approve(w.address, 10).transact({'from': self.w3.eth.accounts[2]})
        self.eth_tester.mine_block()

        w.functions.addSigner(self.w3.eth.accounts[5]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.addSigner(self.w3.eth.accounts[6]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.setThresholds(2, 0).transact({'from': self.w3.eth.accounts[0]})

        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
        r = self.w3.eth.getTransactionReceipt(tx_hash)

        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)
        serial = int(log.data[66:], 16)

        tx_hashh = w.functions.yay(serial).transact({'from': self.w3.eth.accounts[0]})
        r = self.w3.eth.getTransactionReceipt(tx_hashh)
        self.assertEqual(len(r.logs), 0) 

        # attempt to execute fails
        with self.assertRaises(Exception):
            w.functions.executeRequest(serial).transact({'from': self.w3.eth.accounts[0]})

        # dough is still there
        self.assertEqual(t.functions.balanceOf(self.w3.eth.accounts[2]).call(), 100)

        w.functions.yay(serial).transact({'from': self.w3.eth.accounts[5]})
    
        w.functions.executeRequest(serial).transact({'from': self.w3.eth.accounts[0]})

        self.assertEqual(t.functions.balanceOf(self.w3.eth.accounts[2]).call(), 90)
        self.assertEqual(t.functions.balanceOf(self.w3.eth.accounts[3]).call(), 10)

        # additional votes not possible
        with self.assertRaises(Exception):
            w.functions.yay(serial).transact({'from': self.w3.eth.accounts[6]})

        with self.assertRaises(Exception):
            w.functions.nay(serial).transact({'from': self.w3.eth.accounts[6]})

    
    def test_minimal_rejection(self):
        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)

        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
        r = self.w3.eth.getTransactionReceipt(tx_hash)

        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)
        serial = int(log.data[66:], 16)

        tx_hashh = w.functions.nay(serial).transact({'from': self.w3.eth.accounts[0]})
        r = self.w3.eth.getTransactionReceipt(tx_hashh)

        topic_match = rejected_log_signature
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)

        # cannot execute
        with self.assertRaises(Exception):
            w.functions.executeRequest(serial).transact({'from': self.w3.eth.accounts[0]})


    def test_simple_rejection(self):
        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)

        t.functions.approve(w.address, 10).transact({'from': self.w3.eth.accounts[2]})
        self.eth_tester.mine_block()

        w.functions.addSigner(self.w3.eth.accounts[5]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.addSigner(self.w3.eth.accounts[6]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.setThresholds(2, 0).transact({'from': self.w3.eth.accounts[0]})

        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
        r = self.w3.eth.getTransactionReceipt(tx_hash)

        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)
        serial = int(log.data[66:], 16)

        tx_hashh = w.functions.nay(serial).transact({'from': self.w3.eth.accounts[0]})
        r = self.w3.eth.getTransactionReceipt(tx_hashh)
        self.assertEqual(len(r.logs), 0) 

        tx_hashh = w.functions.nay(serial).transact({'from': self.w3.eth.accounts[5]})
        r = self.w3.eth.getTransactionReceipt(tx_hashh)

        topic_match = rejected_log_signature
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)

        # cannot execute
        with self.assertRaises(Exception):
            w.functions.executeRequest(serial).transact({'from': self.w3.eth.accounts[0]})

        # additional votes not possible
        with self.assertRaises(Exception):
            w.functions.yay(serial).transact({'from': self.w3.eth.accounts[6]})

        with self.assertRaises(Exception):
            w.functions.nay(serial).transact({'from': self.w3.eth.accounts[6]})


    def test_veto(self):
        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)

        t.functions.approve(w.address, 10).transact({'from': self.w3.eth.accounts[2]})
        self.eth_tester.mine_block()

        w.functions.addSigner(self.w3.eth.accounts[5]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.addSigner(self.w3.eth.accounts[6]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.setThresholds(2, 1).transact({'from': self.w3.eth.accounts[0]})

        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
        r = self.w3.eth.getTransactionReceipt(tx_hash)

        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)
        serial = int(log.data[66:], 16)

        w.functions.yay(serial).transact({'from': self.w3.eth.accounts[0]})
        tx_hashh = w.functions.nay(serial).transact({'from': self.w3.eth.accounts[5]})
        r = self.w3.eth.getTransactionReceipt(tx_hashh)

        topic_match = vetoed_log_signature
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)

        # cannot execute
        with self.assertRaises(Exception):
            w.functions.executeRequest(serial).transact({'from': self.w3.eth.accounts[0]})

        # additional votes not possible
        with self.assertRaises(Exception):
            w.functions.yay(serial).transact({'from': self.w3.eth.accounts[6]})

        with self.assertRaises(Exception):
            w.functions.nay(serial).transact({'from': self.w3.eth.accounts[6]})


    def test_inflight_change(self):
        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)

        w.functions.addSigner(self.w3.eth.accounts[5]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.addSigner(self.w3.eth.accounts[6]).transact({'from': self.w3.eth.accounts[0]})
        w.functions.setThresholds(2, 0).transact({'from': self.w3.eth.accounts[0]})

        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
        r = self.w3.eth.getTransactionReceipt(tx_hash)

        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)
        serial = int(log.data[66:], 16)

        w.functions.nay(serial).transact({'from': self.w3.eth.accounts[0]})
        w.functions.removeSigner(self.w3.eth.accounts[6]).transact({'from': self.w3.eth.accounts[0]})

        tx_hashh = w.functions.checkResult(serial).transact({'from': self.w3.eth.accounts[6]})
        r = self.w3.eth.getTransactionReceipt(tx_hashh)

        topic_match = rejected_log_signature
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)


if __name__ == '__main__':
    unittest.main()
