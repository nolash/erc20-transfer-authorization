# standard imports
import logging
import unittest

# testutil imports
from tests.base import TestBase

logg = logging.getLogger()


class ERC20TransferAuthorizationTransferTest(TestBase):

    def test_transfer(self):
        w = self.w3.eth.contract(abi=self.abi_wallet, address=self.address_wallet)
        t = self.w3.eth.contract(abi=self.abi_token, address=self.address_token)

        t.functions.approve(w.address, 10).transact({'from': self.w3.eth.accounts[2]})
        self.eth_tester.mine_block()

        tx_hash = w.functions.createRequest(self.w3.eth.accounts[2], self.w3.eth.accounts[3], t.address, 10).transact({'from': self.w3.eth.accounts[9]})
        r = self.w3.eth.getTransactionReceipt(tx_hash)

        topic_match = 'b609ae609609ee99268d05bc1371102cafe8d6b964bf082439ab16be2a01c87c'
        log = r.logs[0]
        topic = log.topics[0]
        self.assertEqual(topic.hex()[2:], topic_match)
        serial = int(log.data[66:], 16)

        w.functions.yay(serial).transact({'from': self.w3.eth.accounts[0]})
        w.functions.executeRequest(serial).transact({'from': self.w3.eth.accounts[0]})

        self.assertEqual(t.functions.balanceOf(self.w3.eth.accounts[2]).call(), 90)
        self.assertEqual(t.functions.balanceOf(self.w3.eth.accounts[3]).call(), 10)

        req = w.functions.requests(1).call()
        self.assertEqual(req[7], self.w3.eth.blockNumber)

        serial_compare = w.functions.requestSenderIndex(self.w3.eth.accounts[2], 0).call()
        self.assertEqual(serial_compare, req[0])

        serial_compare = w.functions.requestRecipientIndex(self.w3.eth.accounts[3], 0).call()
        self.assertEqual(serial_compare, req[0])


if __name__ == '__main__':
    unittest.main()
