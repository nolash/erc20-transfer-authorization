pragma solidity >0.6.11;

// SPDX-License-Identifier: GPL-3.0-or-later

contract ERC20TransferAuthorization {
	struct Transaction {
		uint256 serial;
		address sender;
		address recipient;
		address token;
		uint256 value;
		uint256 yay;
		uint256 nay;
		uint256 blockNumber;
		int8 result; // -1 rejected/vetoed, 0 = completed, 1 = voting, 2 = approved
	}

	mapping ( uint256 => mapping ( address => int8 )) public vote;
	mapping ( uint256 => address[] ) public voters;
	address public owner;
	mapping(uint256 => Transaction) public requests;
	mapping(address => uint256[]) public requestSenderIndex;
	mapping(address => uint256[]) public requestRecipientIndex;
	uint256 hi;
	uint256 lo;
	int256 public count;
	uint256 public quorum;
	uint256 public vetoThreshold;
	uint256 public signerCount;

	mapping(address => bool) public signers;

	event NewRequest(address indexed _sender, address indexed _recipient, address indexed _token, uint256 _value, uint256 _serial);
	event Executed(uint256 _serial);
	event TransferFail(uint256 _serial);
	event QuorumSet(uint256 _quorum, uint256 _vetoThreshold, uint256 _signerCount);
	event SignerAdded(address _signer);
	event SignerRemoved(address _signer);
	event Vetoed(uint256 indexed _serial, uint256 _yays, uint256 _nays);
	event Approved(uint256 indexed _serial, uint256 _yays, uint256 _nays);
	event Rejected(uint256 indexed _serial, uint256 _yays, uint256 _nays);

	constructor() public {
		owner = msg.sender;
		hi = 1;
		lo = 1;
		addSigner(msg.sender);
		setThresholds(1, 0);
	}

	function addSigner(address _signer) public returns (uint256) {
		require(msg.sender == owner, 'ERR_ACCESS');
		require(signers[_signer] == false, 'ERR_NOTFOUND');

		signers[_signer] = true;
		signerCount++;
		emit SignerAdded(_signer);
		return signerCount;
	}

	function removeSigner(address _signer) public returns (uint256) {
		require(msg.sender == owner || msg.sender == _signer, 'ERR_ACCESS');
		require(signers[_signer] == true, 'ERR_NOTFOUND');
		require(signerCount > quorum && signerCount > vetoThreshold, 'ERR_REDUCE_THRESHOLD_FIRST');

		signers[_signer] = false;
		signerCount--;
		emit SignerRemoved(_signer);
		return signerCount;
	}

	function setThresholds(uint256 _quorum, uint256 _vetoThreshold) public returns (bool) {
		require(_quorum <= signerCount);
		require(_quorum > 0);
		require(_vetoThreshold <= signerCount);

		quorum = _quorum;
		vetoThreshold = _vetoThreshold;
		emit QuorumSet(quorum, vetoThreshold, signerCount);
		return true;
	}

	// create new request
	function createRequest(address _sender, address _recipient, address _token, uint256 _value) public returns (uint256) {
		Transaction storage txx = requests[hi];

		txx.serial = hi;
		txx.recipient = _recipient;
		txx.sender = _sender;
		txx.token = _token;
		txx.value = _value;
		txx.result = 1;

		count++;	
		hi++;

		emit NewRequest(txx.sender, txx.recipient, txx.token, txx.value, txx.serial);

		return txx.serial;
	}

	// if request was oldest in index, move the pointer to oldest request to next oldest unfinished request.
	// if no unfinished requests exits, it will point to newest request
	function removeItem(uint256 _serialToRemove) private returns (uint256) {
		count--;

		if (count > 0) {
			if (_serialToRemove == lo) {
				uint256 i;
				i = getSerialAt(0);
				if (i == 0) {
					lo = hi;
				} else {
					lo = i;
				}
			}
		} else {
			lo = hi;
		}

		return lo;
	}

	// index of newest vote
	function lastSerial() public view returns ( uint256 ) {
		return hi - 1;
	}

	// index of oldest unfinished vote
	function nextSerial() public view returns ( uint256 ) {
		if (hi - lo == 0) {
			return 0;
		}
		return lo;
	}

	// get the unfinished vote at the given index
	function getSerialAt(uint256 _idx) public view returns ( uint256 ) {
		uint256 i;
		for (uint256 j = lo; j < hi; j++) {
			Transaction storage txx = requests[j];
			if (txx.result > 0) {
				if (i == _idx) {
					return txx.serial;
				}
				i++;
			}
		}
		return 0;
	}

	// vote yay, one per signer
	function yay(uint256 _serial) public returns (uint256) {
		require(signers[msg.sender], 'ERR_ACCESS');
		require(vote[_serial][msg.sender] == 0, 'ERR_ALREADYVOTED');

		Transaction storage txx = requests[_serial];
		require(txx.result == 1);

		vote[txx.serial][msg.sender] = 1;
		voters[txx.serial].push(msg.sender);
		txx.yay++;

		checkResult(txx.serial);	

		return txx.yay;
	}

	// vote nay, one per signer
	function nay(uint256 _serial) public returns (uint256) {
		require(signers[msg.sender], 'ERR_ACCESS');
		require(vote[_serial][msg.sender] == 0, 'ERR_ALREADYVOTED');

		Transaction storage txx = requests[_serial];
		require(txx.result == 1);

		vote[txx.serial][msg.sender] = -1;
		voters[txx.serial].push(msg.sender);
		txx.nay++;

		checkResult(txx.serial);	

		return txx.nay;
	}

	// locks the state of the vote if quorum or veto is reached
	// returns true if state changes
	function checkResult(uint256 _serial) public returns (bool) {
		bool result;
		Transaction storage txx = requests[_serial];

		if (txx.result < 1) {
			return result;
		}

		if (txx.yay >= quorum) {
			txx.result = 2;
			emit Approved(txx.serial, txx.yay, txx.nay);
			result = true;
		} else if (vetoThreshold > 0 && txx.nay >= vetoThreshold) {
			txx.result = -1;
			removeItem(txx.serial);
			emit Vetoed(txx.serial, txx.yay, txx.nay);
			result = true;
		} else if (signerCount - txx.nay < quorum) { 
			txx.result = -1;
			removeItem(txx.serial);
			emit Rejected(txx.serial, txx.yay, txx.nay);
			result = true;
		}

		return result;
	}

	// execute transfer. needs positive vote result
	function executeRequest(uint256 _serial) public returns (bool) {
		Transaction storage txx = requests[_serial];

		require(txx.serial > 0, 'ERR_INVALID_REQUEST');
		require(txx.result == 2, 'ERR_NOT_ENDORSED');

		txx.result = 0;

		(bool success, bytes memory _r) = txx.token.call(abi.encodeWithSignature("transferFrom(address,address,uint256)", txx.sender, txx.recipient, txx.value));

		removeItem(txx.serial);

		txx.blockNumber = block.number;
		requestSenderIndex[txx.sender].push(txx.serial);
		requestRecipientIndex[txx.recipient].push(txx.serial);
		if (success) {
			emit Executed(_serial);
		} else {
			emit TransferFail(_serial);
		}

		return success;
	}
}
